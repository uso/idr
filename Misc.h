//---------------------------------------------------------------------------
#ifndef MiscH
#define MiscH
//---------------------------------------------------------------------------
#include "Decompiler.h"
//---------------------------------------------------------------------------
//Float Type
#define     FT_SINGLE       1
#define     FT_DOUBLE       2
#define     FT_EXTENDED     3
#define     FT_REAL         4
#define     FT_COMP         5
//---------------------------------------------------------------------------
//global API
void __fastcall ScaleForm(TForm* AForm);
int __fastcall Adr2Pos(DWORD adr);
System::AnsiString __fastcall Val2Str0(DWORD Val);
System::AnsiString __fastcall Val2Str1(DWORD Val);
System::AnsiString __fastcall Val2Str2(DWORD Val);
System::AnsiString __fastcall Val2Str4(DWORD Val);
System::AnsiString __fastcall Val2Str5(DWORD Val);
System::AnsiString __fastcall Val2Str8(DWORD Val);
DWORD __fastcall Pos2Adr(int Pos);
PInfoRec __fastcall AddToBSSInfos(DWORD Adr, System::AnsiString AName, System::AnsiString ATypeName);
void __fastcall AddClassAdr(DWORD Adr, const System::AnsiString& AName);
void __fastcall AddFieldXref(PFIELDINFO fInfo, DWORD ProcAdr, int ProcOfs, char type);
void __fastcall AddPicode(int Pos, BYTE Op, System::AnsiString Name, int Ofs);
int __fastcall ArgsCmpFunction(void *item1, void *item2);
int __fastcall BranchGetPrevInstructionType(DWORD fromAdr, DWORD* jmpAdr, PLoopInfo loopInfo);
bool __fastcall CanReplace(const System::AnsiString& fromName, const System::AnsiString& toName);
void __fastcall ClearFlag(DWORD flag, int pos);
void __fastcall ClearFlags(DWORD flag, int pos, int num);
void __fastcall Copy2Clipboard(TStrings* items, int leftMargin, bool asmCode);
int __fastcall ExportsCmpFunction(void *item1, void *item2);
System::AnsiString __fastcall ExtractClassName(const System::AnsiString& AName);
System::AnsiString __fastcall ExtractProcName(const System::AnsiString& AName);
System::AnsiString __fastcall ExtractName(const System::AnsiString& AName);
System::AnsiString __fastcall ExtractType(const System::AnsiString& AName);
void __fastcall FillArgInfo(int k, BYTE callkind, PARGINFO argInfo, BYTE** p, int* s);
DWORD __fastcall FindClassAdrByName(const System::AnsiString& AName);
int __fastcall FloatNameToFloatType(System::AnsiString AName);
System::AnsiString __fastcall GetArrayElementType(System::AnsiString arrType);
int __fastcall GetArrayElementTypeSize(System::AnsiString arrType);
bool __fastcall GetArrayIndexes(System::AnsiString arrType, int ADim, int* LowIdx, int* HighIdx);
int __fastcall GetArraySize(System::AnsiString arrType);
DWORD __fastcall GetChildAdr(DWORD Adr);
DWORD __fastcall GetClassAdr(const System::AnsiString& AName);
int __fastcall GetClassSize(DWORD adr);
System::AnsiString __fastcall GetClsName(DWORD adr);
System::AnsiString __fastcall GetDefaultProcName(DWORD adr);
System::AnsiString __fastcall GetDynaInfo(DWORD adr, WORD id, DWORD* dynAdr);
System::AnsiString __fastcall GetDynArrayTypeName(DWORD adr);
System::AnsiString __fastcall GetEnumerationString(System::AnsiString TypeName, Variant Val);
System::AnsiString __fastcall GetImmString(int Val);
System::AnsiString __fastcall GetImmString(System::AnsiString TypeName, int Val);
PInfoRec __fastcall GetInfoRec(DWORD adr);
int __fastcall GetLastLocPos(int fromAdr);
System::AnsiString __fastcall GetModuleVersion(const System::AnsiString& module);
int __fastcall GetNearestArgA(int fromPos);
int __fastcall GetNearestDownInstruction(int fromPos);
int __fastcall GetNearestDownInstruction(int fromPos, char* Instruction);
int __fastcall GetNearestUpPrefixFs(int fromPos);
int __fastcall GetNearestUpInstruction(int fromPos);
int __fastcall GetNthUpInstruction(int fromPos, int N);
int __fastcall GetNearestUpInstruction(int fromPos, int toPos);
int __fastcall GetNearestUpInstruction(int fromPos, int toPos, int no);
int __fastcall GetNearestUpInstruction1(int fromPos, int toPos, char* Instruction);
int __fastcall GetNearestUpInstruction2(int fromPos, int toPos, char* Instruction1, char* Instruction2);
DWORD __fastcall GetParentAdr(DWORD Adr);
System::AnsiString __fastcall GetParentName(DWORD adr);
System::AnsiString __fastcall GetParentName(const System::AnsiString& ClassName);
int __fastcall GetParentSize(DWORD Adr);
int __fastcall GetProcRetBytes(MProcInfo* pInfo);
int __fastcall GetProcSize(DWORD fromAdr);
int __fastcall GetRecordSize(System::AnsiString AName);
System::AnsiString __fastcall GetRecordFields(int AOfs, System::AnsiString AType);
System::AnsiString __fastcall GetAsmRegisterName(int Idx);
System::AnsiString __fastcall GetDecompilerRegisterName(int Idx);
System::AnsiString __fastcall GetSetString(System::AnsiString TypeName, BYTE* ValAdr);
DWORD __fastcall GetStopAt(DWORD vmtAdr);
DWORD __fastcall GetOwnTypeAdr(System::AnsiString AName);
PTypeRec __fastcall GetOwnTypeByName(System::AnsiString AName);
System::AnsiString __fastcall GetTypeDeref(System::AnsiString ATypeName);
BYTE __fastcall GetTypeKind(System::AnsiString AName, int* size);
int __fastcall GetPackedTypeSize(System::AnsiString AName);
System::AnsiString __fastcall GetTypeName(DWORD TypeAdr);
int __fastcall GetTypeSize(System::AnsiString AName);
int __fastcall ImportsCmpFunction(void *item1, void *item2);
bool __fastcall IsADC(int Idx);
int __fastcall IsBoundErr(DWORD fromAdr);
bool __fastcall IsConnected(DWORD fromAdr, DWORD toAdr);
bool __fastcall IsBplByExport(const char* bpl);
bool __fastcall IsDefaultName(System::AnsiString AName);
DWORD __fastcall IsGeneralCase(DWORD fromAdr, int retAdr);
bool __fastcall IsExit(DWORD fromAdr);
bool __fastcall IsInheritsByAdr(const DWORD Adr1, const DWORD Adr2);
bool __fastcall IsInheritsByClassName(const System::AnsiString& Name1, const System::AnsiString& Name2);
bool __fastcall IsInheritsByProcName(const System::AnsiString& Name1, const System::AnsiString& Name2);
int __fastcall IsInitStackViaLoop(DWORD fromAdr, DWORD toAdr);
bool __fastcall IsSameRegister(int Idx1, int Idx2);
bool __fastcall IsValidCodeAdr(DWORD Adr);
bool __fastcall IsValidCString(int pos);
bool __fastcall IsValidImageAdr(DWORD Adr);
bool __fastcall IsValidName(int len, int pos);
bool __fastcall IsValidString(int len, int pos);
bool __fastcall IsXorMayBeSkipped(DWORD fromAdr);
void __fastcall MakeGvar(PInfoRec recN, DWORD adr, DWORD xrefAdr);
System::AnsiString __fastcall MakeGvarName(DWORD adr);
int __fastcall MethodsCmpFunction(void *item1, void *item2);
void __fastcall OutputDecompilerHeader(FILE* f);
bool __fastcall IsFlagSet(DWORD flag, int pos);
void __fastcall SetFlag(DWORD flag, int pos);
void __fastcall SetFlags(DWORD flag, int pos, int num);
int __fastcall SortUnitsByAdr(void *item1, void* item2);
int __fastcall SortUnitsByNam(void *item1, void* item2);
int __fastcall SortUnitsByOrd(void *item1, void* item2);
System::AnsiString __fastcall TransformString(char* str, int len);
System::AnsiString __fastcall TransformUString(WORD codePage, wchar_t* data, int len);
System::AnsiString __fastcall TrimTypeName(const System::AnsiString& TypeName);
System::AnsiString __fastcall TypeKind2Name(BYTE kind);
System::AnsiString __fastcall UnmangleName(System::AnsiString Name);
//Decompiler
int __fastcall IsAbs(DWORD fromAdr);
int _fastcall IsIntOver(DWORD fromAdr);
int __fastcall IsInlineLengthCmp(DWORD fromAdr);
int __fastcall IsInlineLengthTest(DWORD fromAdr);
int __fastcall IsInlineDiv(DWORD fromAdr, int* div);
int __fastcall IsInlineMod(DWORD fromAdr, int* mod);
int __fastcall ProcessInt64Equality(DWORD fromAdr, DWORD* maxAdr);
int __fastcall ProcessInt64NotEquality(DWORD fromAdr, DWORD* maxAdr);
int __fastcall ProcessInt64Comparison(DWORD fromAdr, DWORD* maxAdr);
int __fastcall ProcessInt64ComparisonViaStack1(DWORD fromAdr, DWORD* maxAdr);
int __fastcall ProcessInt64ComparisonViaStack2(DWORD fromAdr, DWORD* maxAdr);
int __fastcall IsInt64Equality(DWORD fromAdr, int* skip1, int* skip2, bool *immVal, __int64* Val);
int __fastcall IsInt64NotEquality(DWORD fromAdr, int* skip1, int* skip2, bool *immVal, __int64* Val);
int __fastcall IsInt64Comparison(DWORD fromAdr, int* skip1, int* skip2, bool *immVal, __int64* Val);
int __fastcall IsInt64ComparisonViaStack1(DWORD fromAdr, int* skip1, DWORD* simEnd);
int __fastcall IsInt64ComparisonViaStack2(DWORD fromAdr, int* skip1, int* skip2, DWORD* simEnd);
int __fastcall IsInt64Shr(DWORD fromAdr);
int __fastcall IsInt64Shl(DWORD fromAdr);
System::AnsiString __fastcall InputDialogExec(System::AnsiString caption, System::AnsiString labelText, System::AnsiString text);
System::AnsiString __fastcall ManualInput(DWORD procAdr, DWORD curAdr, System::AnsiString caption, System::AnsiString labelText);
bool __fastcall MatchCode(BYTE* code, MProcInfo* pInfo);
void __fastcall SaveCanvas(TCanvas* ACanvas);
void __fastcall RestoreCanvas(TCanvas* ACanvas);
void __fastcall DrawOneItem(System::AnsiString AItem, TCanvas* ACanvas, TRect &ARect, TColor AColor, int flags);
int __fastcall IsTryBegin(DWORD fromAdr, DWORD* endAdr);
int __fastcall IsTryBegin0(DWORD fromAdr, DWORD* endAdr);
int __fastcall IsTryEndPush(DWORD fromAdr, DWORD* endAdr);
int __fastcall IsTryEndJump(DWORD fromAdr, DWORD* endAdr);
//---------------------------------------------------------------------------
#endif
